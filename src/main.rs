extern crate rustc_serialize;
extern crate sodiumoxide;
extern crate cbor;

mod without_tag;
mod with_tag;

use rustc_serialize::{Decodable, Encodable};

fn main() {
    {  // Without using tags
        let revocation_id = without_tag::RevocationId::new();
        let other_id = without_tag::OtherId::new();

        let mut encoder = cbor::Encoder::from_memory();
        // We can encode multiple instances here, since they're all the same type (i.e. they're all
        // wrapped in the enum)
        encoder.encode(&[without_tag::MaidSafeType::Revocation(revocation_id.clone()),
                         without_tag::MaidSafeType::Other(other_id.clone())]).unwrap();

        let mut decoder = cbor::Decoder::from_bytes(encoder.as_bytes());

        let handler = |maidsafe_type| {
            // We're decoding the same type as we encoded, i.e. the enum type.
            match maidsafe_type {
                without_tag::MaidSafeType::Revocation(id) => {
                    println!("Handling \"without_tag\" revocation type.");
                    assert_eq!(revocation_id, id)
                },
                without_tag::MaidSafeType::Other(id) => {
                    println!("Handling \"without_tag\" other type.");
                    assert_eq!(other_id, id)
                }
            };
        };
        handler(decoder.decode().next().unwrap().unwrap());
        handler(decoder.decode().next().unwrap().unwrap());
    }





    {  // Using tags
        let revocation_id = with_tag::RevocationId::new();
        let other_id = with_tag::OtherId::new();

        let handler = |maidsafe_type| {
            // We're decoding a different type than what we encoded, i.e. encode basic type and
            // decode the enum type.
            match maidsafe_type {
                with_tag::MaidSafeType::Revocation(id) => {
                    println!("Handling \"with_tag\" revocation type.");
                    assert_eq!(revocation_id, id)
                },
                with_tag::MaidSafeType::Other(id) => {
                    println!("Handling \"with_tag\" other type.");
                    assert_eq!(other_id, id)
                }
            };
        };

        // We can't encode multiple instances since we're encoding as the basic type (i.e. not
        // wrapped in the enum).
        {
            let mut encoder = cbor::Encoder::from_memory();
            encoder.encode(&[revocation_id.clone()]).unwrap();

            let mut decoder = cbor::Decoder::from_bytes(encoder.as_bytes());
            handler(decoder.decode().next().unwrap().unwrap());
        }
        {
            let mut encoder = cbor::Encoder::from_memory();
            encoder.encode(&[other_id.clone()]).unwrap();

            let mut decoder = cbor::Decoder::from_bytes(encoder.as_bytes());
            handler(decoder.decode().next().unwrap().unwrap());
        }
    }
}
