use sodiumoxide::crypto::sign::{gen_keypair, PublicKey, SecretKey};

// We can't perform any custom action during parsing, i.e. validation of data.  This could be done
// after parsing as a MaidSafeType though.





#[derive(Clone, Debug, Eq, PartialEq, RustcEncodable, RustcDecodable)]
pub struct RevocationId {
    type_tags: (u64, u64, u64),
    public_key: PublicKey,
    secret_key: SecretKey
}

impl RevocationId {
    pub fn new() -> RevocationId {
        let key_pair = gen_keypair();
        RevocationId{ type_tags: (1, 2, 3), public_key: key_pair.0, secret_key: key_pair.1 }
    }
}





#[derive(Clone, Debug, Eq, PartialEq, RustcEncodable, RustcDecodable)]
pub struct OtherId {
    type_tags: (u64, u64, u64),
    public_key: PublicKey,
    secret_key: SecretKey
}

impl OtherId {
    pub fn new() -> OtherId {
        let key_pair = gen_keypair();
        OtherId{ type_tags: (4, 5, 6), public_key: key_pair.0, secret_key: key_pair.1 }
    }
}





#[derive(Clone, Debug, Eq, PartialEq, RustcEncodable, RustcDecodable)]
pub enum MaidSafeType {
    Revocation(RevocationId),
    Other(OtherId),
}
