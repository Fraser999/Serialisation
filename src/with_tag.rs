use cbor::CborTagEncode;
use rustc_serialize::{Decodable, Decoder, Encodable, Encoder};
use sodiumoxide::crypto::sign::{gen_keypair, PublicKey, SecretKey};





#[derive(Clone, Debug, Eq, PartialEq)]
pub struct RevocationId {
    type_tags: (u64, u64, u64),
    public_key: PublicKey,
    secret_key: SecretKey
}

impl RevocationId {
    pub fn new() -> RevocationId {
        let key_pair = gen_keypair();
        RevocationId{ type_tags: (7, 8, 9), public_key: key_pair.0, secret_key: key_pair.1 }
    }
}

// We encode using CborTagEncode, but don't consume the tag when decoding since that's used in the
// wrapper enum.  While valid, this is contrary to the example in the CBOR docs.
//
// Also, there's a bug in CBOR which currently prohibits us from encoding/decoding any `u64` via
// the CborTagEncode method.
impl Encodable for RevocationId {
    fn encode<E: Encoder>(&self, encoder: &mut E)->Result<(), E::Error> {
        CborTagEncode::new(self.type_tags.0,
            &(&self.public_key, &self.secret_key)).encode(encoder)
    }
}

impl Decodable for RevocationId {
    fn decode<D: Decoder>(decoder: &mut D)-> Result<RevocationId, D::Error> {
        let (public_key, secret_key): (PublicKey, SecretKey) = try!(Decodable::decode(decoder));

        // Validate parsed types for security

        Ok(RevocationId{ type_tags: (7, 8, 9), public_key: public_key, secret_key: secret_key })
    }
}





#[derive(Clone, Debug, Eq, PartialEq)]
pub struct OtherId {
    type_tags: (u64, u64, u64),
    public_key: PublicKey,
    secret_key: SecretKey
}

impl OtherId {
    pub fn new() -> OtherId {
        let key_pair = gen_keypair();
        OtherId{ type_tags: (10, 11, 12), public_key: key_pair.0, secret_key: key_pair.1 }
    }
}

impl Encodable for OtherId {
    fn encode<E: Encoder>(&self, encoder: &mut E)->Result<(), E::Error> {
        CborTagEncode::new(self.type_tags.0,
            &(&self.public_key, &self.secret_key)).encode(encoder)
    }
}

impl Decodable for OtherId {
    fn decode<D: Decoder>(decoder: &mut D)-> Result<OtherId, D::Error> {
        let (public_key, secret_key): (PublicKey, SecretKey) = try!(Decodable::decode(decoder));

        // Validate parsed types for security

        Ok(OtherId{ type_tags: (10, 11, 12), public_key: public_key, secret_key: secret_key })
    }
}





#[derive(Clone, Debug, Eq, PartialEq)]
pub enum MaidSafeType {
    Revocation(RevocationId),
    Other(OtherId),
}

impl Decodable for MaidSafeType {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<MaidSafeType, D::Error> {
        let tag = try!(decoder.read_u64());

        match tag {
            7 => Ok(MaidSafeType::Revocation(try!(Decodable::decode(decoder)))),
            10 => Ok(MaidSafeType::Other(try!(Decodable::decode(decoder)))),
            _ => panic!("Error")
        }
    }
}
